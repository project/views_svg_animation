<?php declare(strict_types = 1);
$files = [];

$viewThemeFiles = [
  __DIR__ . '/../../../core/modules/views/views.theme.inc',
  __DIR__ . '/web/core/modules/views/views.theme.inc',
  getcwd() . '/web/core/modules/views/views.theme.inc',
  getcwd() . '/../../../core/modules/views/views.theme.inc',
];
foreach ($viewThemeFiles as $viewThemeFile) {
  if (file_exists($viewThemeFile)) {
    $files[] = $viewThemeFile;
  }
}
return [
  'parameters' => [
    'scanFiles' => $files,
  ],
];
